load("galen-bootstrap/galen-bootstrap.js");

// $galen.settings.website = "https://arrow-seneca.ubu.carbon8test.com/contact-us";
// $galen.settings.website = "https://ascent360.ubu.carbon8test.com/about/contact";

// $galen.registerDevice("mobile", inSingleBrowser("mobile emulation", "450x700", ["mobile"]));
// $galen.registerDevice("tablet", inSingleBrowser("tablet emulation", "600x700", ["tablet"]));
// $galen.registerDevice("desktop", inSingleBrowser("desktop emulation", "1024x768", ["desktop"]));
// $galen.registerDevice("desktop-fullhd", inSingleBrowser("desktop extra emulation", "1920x1080", ["desktop-fullhd"]));

// BrowserStack
$galen.registerDevice(
  "desktop",
  inSeleniumGrid(
    "http://liemnguyen3:TU4y6M2WFvrC5uD3syT3@hub.browserstack.com/wd/hub",
    "1024x768",
    ["desktop"],
    {
      desiredCapabilities: {
        'os' : 'Windows',
        'os_version' : '10',
        'browserName' : 'Chrome',
        'browser_version' : '43.0',
        'browserstack.local' : 'true',
        'browserstack.video' : 'true',
        "browserstack.debug": "true",
        'browserstack.selenium_version' : '3.5.2',
        'browserstack.user' : 'liemnguyen3',
        'browserstack.key' : 'TU4y6M2WFvrC5uD3syT3'
      }
    }
  )
);

$galen.registerDevice(
  "mobile",
  inSeleniumGrid(
    "http://liemnguyen3:TU4y6M2WFvrC5uD3syT3@hub.browserstack.com/wd/hub",
    "1440x900",
    ["mobile"],
    {
      desiredCapabilities: {
        'os' : 'Windows',
        'os_version' : '10',
        'browserName' : 'Edge',
        'browser_version' : '16.0',
        'browserstack.local' : 'true',
        'browserstack.video' : 'true',
        "browserstack.debug": "true",
        'browserstack.selenium_version' : '3.5.2',
        'browserstack.user' : 'liemnguyen3',
        'browserstack.key' : 'TU4y6M2WFvrC5uD3syT3'
      }
    }
  )
);



// $galen.registerDevice("desktop-fullhd", inLocalBrowser("desktop extra emulation", "1920x1080", ["desktop-fullhd"]));