/*eslint-disable */
rule("co style la %{obj1}, %{obj2}, %{obj3}, %{obj4}, %{obj5}", function (objectName, parameters) {
  var styles = ["font-family", "font-size", "font-weight", "color", "line-height"]
  var params = [parameters.obj1, parameters.obj2, parameters.obj3, parameters.obj4, parameters.obj5]
  if(styles.length > 0) {
    for(var i = 0 ; i < styles.length; i++) {
      this.addSpecs([
        "css " + styles[i] + " is " + params[i]
      ])
    }
  }
})