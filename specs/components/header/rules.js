/* eslint-disable */
rule("%{obj1} should be left %{obj2}", function (objectName, parameters) {
  var Object1 = parameters.obj1
  var Object2 = parameters.obj2
  this.addObjectSpecs(Object1, [
    "aligned vertically left " +  Object2 + " 1px"
  ])
})