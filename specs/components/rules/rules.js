/* eslint-disable */
rule("co hien thi", function (objectName, parameters) {
  this.addSpecs([
    "visible"
  ])
})
rule("co do rong %{size}", function (objectName, parameters) {
  var object2 = parameters.size
  this.addSpecs([
    "width " + object2 +" % of container/width "
  ])
})
rule("co do dai khong qua %{size}", function (objectName, parameters) {
  var object2 = parameters.size
  this.addSpecs([
    "width < " + object2
  ])
})
rule("co do rong bang voi %{obj2}", function (objectName, parameters) {
  var object2 = parameters.obj2
  var widthObj1 = find(objectName).width();
  if(widthObj1 == find(object2).width()) {
    this.addSpecs([
      "aligned vertically all " + object2
    ])
  }
})


rule("%{objectPattern} co do rong so voi %{obj2} la %{size}", function(objectName, parameters) {
  var items= findAll(parameters.objectPattern)
  var obj2 = parameters.obj2
  var size = parameters.size
  if(items.length > 0) {
    for(var i= 0; i < items.length; i++) {
      this.addObjectSpecs(items[i].name, 
        [ "on top left edge " + obj2 + " "+  size +" px left"]
      );
    }
  }
})
rule("cach %{position} voi %{obj2} %{operator} %{size}", function (objectName, parameters) {
  var position = parameters.position
  var object2 = parameters.obj2
  var operator = parameters.operator
  var size = parameters.size
  if ( position == 'trai') {
      position = 'left-of'
  } else if ( position == 'phai') {
    position = 'right-of'
  }
  this.addSpecs([
    position + " " + object2 + " " + operator + " " + size
  ])
})
rule("canh %{position} voi %{obj2}", function (objectName, parameters) {
  var Object2 = parameters.obj2
  var position = parameters.position
  if (position == 'trai') {
    position = 'left'
  } else if (position == 'phai') {
    position = 'right'
  }
  this.addSpecs([
    "aligned vertically "+ position +" " + Object2
  ])
})
rule("canh top voi %{obj2}", function (objectName, parameters) {
  var Object2 = parameters.obj2
  this.addSpecs([
    "aligned horizontally top " +  Object2
  ])
})
rule("%{obj1} cach %{obj2} it nhat %{obj3}", function (objectName, parameters) {
  var Object1 = parameters.obj1
  var Object2 = parameters.obj2
  var Object3 = parameters.obj3
  this.addObjectSpecs(Object1, [
    "left-of " +  Object2 + ' > ' + Object3
  ])
})
rule("%{elements} cach nhau %{operator} %{size}", function (objectName, parameters) {
  var elements = parameters.elements
  var operator = parameters.operator
  var size = parameters.size
  if(elements.length > 0) {
    for(var i = 0; i < elements.length - 1; i++) {
      this.addObjectSpecs(elements[i].name, [
        "near" + elements[i + 1].name + ' ' + operator + " " + size
      ])
    }
  }
})

rule("%{elements} co mau lan luot la %{colors}", function (objectName, parameters) {
  var elements = parameters.elements
  var colorsArr = parameters.colors.split(', ')
  for( var i= 0; i < colorsArr.length; i++) {
    colorsArr[i] = colorsArr[i].trim(' ')
  }
  if(elements.length > 0) {
    for(var i = 0; i < elements.length; i++) {
      this.addObjectSpecs(elements[i].name, [
        "css background-color is " + colorsArr[i]
      ])
    }
  }
})
rule("%{objectPattern} canh %{position} voi nhau", function(objectName, parameters) {
  var items = findAll(parameters.objectPattern);
  var position = parameters.position;
  var pos = position;
  var aligned = '';
  if (position == "left" || position == "right" || position == "trai" || position == "phai") {
    aligned = 'vertically';
    if (position == "trai") {
      pos = "left"
    }
    if (position == "phai") {
      pos = "right"
    }
  } else {
    if (position == "tren") {
      pos = "top"
    }
    if (position == "duoi") {
      pos = "bottom"
    }
    aligned = 'horizontally';
  }
  if (items.length > 0) {
    for (var i = 0; i < items.length; i++) {
      this.addObjectSpecs(items[i].name, 
        [ "aligned " + aligned + " " + pos + " " + items[0].name ]
      );
    }
  } else {
      throw new Error("Couldn't find any items matching " + parameters.objectPattern);
  }
})

rule("%{objectPattern} co khoang cach bang nhau", function(objectName, parameters) {
  var items = findAll(parameters.objectPattern);
  var height = Math.abs(items[1].top() + items[1].height() - items[0].top());
  if (items.length > 0) {
    for (var i = 0; i < items.length - 1; i += 2) {
      this.addObjectSpecs(items[i].name, 
        [ "below " + items[i + 1].name + " " + height + "px" ]
      );
    }
  } else {
      throw new Error("Couldn't find any items matching " + parameters.objectPattern);
  }
})

